import os
import sys
import django
import smtplib
import send_mail 

sys.path.insert(0,'/opt/stock_centro/buffet/')
sys.path.insert(1,'/opt/stock_centro/')
os.environ["DJANGO_SETTINGS_MODULE"] = "buffet.settings"
django.setup()

from ventas.models import *
from producto.models import *
from datetime import *

from django.contrib.auth.models import User
emails = []
administradores = User.objects.all()
for a in administradores:
    if a.is_superuser:
        emails.append(a.email) 


#Este script se ejecuta diariamente.

hoy = date.today()
if (hoy.month>0 and hoy.month < 10):
    mes ="0"+str(hoy.month) 
else:
    mes = str(hoy.month)
if(hoy.day >0 and hoy.day<10):
    dia="0"+str(hoy.day)
else:
    dia=str(hoy.day)
hoy = str(hoy.year) +"-"+mes+"-"+dia
#ventas_de_hoy = Venta.objects.filter(fecha__year = str(hoy.year), fecha__month = str(hoy.month), fecha__day = str(hoy.day-1)) #Obtengo las ventas de hoy. 

ventas_de_hoy=Venta.objects.filter(fecha__startswith = hoy)
if (ventas_de_hoy.count() > 0):

    c = Caja()
    costo = 0
    venta = 0
    for v in ventas_de_hoy:
        prod_venta = ProductoVenta.objects.filter(venta=v)
        for pv in prod_venta: #obtengo los productos de cada venta y calculo los costos totales.
            costo = (pv.producto.precio_costo*pv.cantidad)+costo
        c.total = v.monto_total + c.total 
        c.ganancia = c.total - costo
    c.costo = costo
    c.descripcion = "-"
    c.fecha = hoy
    c.save()

    asunto = 'Cierre de caja: ' + str(datetime.now().year) + '-'+ str(datetime.now().month) + '-'+ str(datetime.now().day)
    mensaje = 'Resumen del día: \nTotal ventas: $' + "%.*f" % (2, c.total) + "\nGanancia: $"	+ "%.*f" % (2, c.ganancia)	
    for e in emails:
        send_mail.send_mail(e, mensaje, asunto)


#Lo que sigue hay que agregarlo al cron para ejecutar este script. Esto va a correr de lunes a viernes a las 23 horas.
#00 23 * * * root python /[ruta_de_proyecto]/scripts/py/cierre_caja.py 



