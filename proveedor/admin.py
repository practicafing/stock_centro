from django.contrib import admin
from .models import *
from django.contrib import messages
import datetime



#inline de los productos por pedido
class ProductoPedidoAdmin(admin.TabularInline):
    model = ProductoPedido
    raw_id_fields = ("producto",)
    extra=1



def llego_pedido(modeladmin, request, queryset):
    for obj in queryset:
        if obj.llego == False:
            obj.llego = True
            for p in obj.productopedido_set.all():
                p.producto.stock += p.cantidad_pedida
                p.producto.save()
            obj.save()
        else:
            messages.error(request, "El pedido " + str(obj.fecha) + " ya ha llegado. ID: " + str(obj.id) )

llego_pedido.short_description = 'Llego pedido'

def repetir_pedido(modeladmin, request, queryset):
    for obj in queryset:
        pr = Pedido()
        pr.fecha = datetime.datetime.now()
        pr.proveedor = obj.proveedor
        pr.usuario_pidio = request.user
        pr.descripcion = 'Pedido repetido de ID:' + str(obj.id) + ' Fecha: ' + str(obj.fecha)
        pr.save()

        for p in obj.productopedido_set.all():
            pp = ProductoPedido()
            pp.pedido = pr
            pp.producto = p.producto
            pp.cantidad_pedida = p.cantidad_pedida
            pp.save()

repetir_pedido.short_description = 'Repetir pedido'

class PedidoAdmin(admin.ModelAdmin):
    inlines = [ProductoPedidoAdmin,]
    list_display = ['fecha', 'proveedor', 'usuario_pidio', 'llego' ]
    actions = [ llego_pedido, repetir_pedido, ]

    def save_model(self, request, obj, form, change):
        #aca guardarmos el usuario que creo la venta
        #if change == False:
        obj.usuario_pidio = request.user
        super().save_model(request, obj, form, change)

admin.site.register(Proveedor)
admin.site.register(Pedido, PedidoAdmin)