from django.db import models
from producto.models import Producto
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

# Create your models here.
class Proveedor(models.Model):
    nombre = models.CharField(max_length=240, help_text='Nombre y apellido del proveedor.',)
    tel = models.CharField(max_length=240, null = True, blank = True)
    direccion = models.CharField(max_length=240, null = True, blank = True)
    #productos relacionados

    def __str__(self):
        return self.nombre

class Pedido(models.Model):
    fecha = models.DateField(auto_now_add=True)
    proveedor = models.ForeignKey(Proveedor, on_delete='CASCADE', null=True, blank=True)
    llego = models.BooleanField(default=False, editable=False, )
    usuario_pidio = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, editable=False)
    descripcion = models.TextField( max_length = 400 , blank=True, null=True)

class ProductoPedido(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad_pedida = models.IntegerField()
    pedido = models.ForeignKey(Pedido, on_delete=models.CASCADE)

    def clean(self):
        if self.cantidad_pedida < 1:
            raise ValidationError('Cargue una cantidad válida.')
