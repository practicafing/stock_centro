from django.db import models
#from ventas.models import *
from django.core.exceptions import ValidationError

class Producto(models.Model):
	codigo = models.CharField(max_length=20, unique=True, blank=True, null=True)
	nombre = models.CharField(max_length=200)
	precio_costo = models.FloatField(null=True, blank=True, default=0.0)
	precio_venta = models.FloatField(null=True, blank=True, default=0.0)
	stock = models.IntegerField(default=0)
	#venta = models.ForeignKey('ventas.Venta', null=True,on_delete=models.CASCADE,)
	
	def __str__(self):
		return self.nombre + ' (' + str(self.stock) + ')'

	def clean(self):
		if self.stock < 0:
			raise ValidationError('No puede haber stock negativo')