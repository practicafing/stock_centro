from django.contrib import admin

# Register your models here.
from .models import Producto

class ProductoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'codigo', 'precio_costo', 'precio_venta', 'stock']
    search_fields = ['nombre', 'codigo']

admin.site.register(Producto, ProductoAdmin)