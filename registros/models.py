from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in, user_logged_out

# Create your models here.
class Registro(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, editable=False)
    fecha_hora = models.DateTimeField(auto_now_add=True)
    entro = models.BooleanField()

def registro_login_usuario(sender, user, request, **kwargs):
    r = Registro()
    r.usuario = user
    r.entro = True
    r.save()

def registro_logout_usuario(sender, user, request, **kwargs):
    r = Registro()
    r.usuario = user
    r.entro = False
    r.save()

user_logged_in.connect(registro_login_usuario)
user_logged_out.connect(registro_logout_usuario)