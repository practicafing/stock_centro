from django.contrib import admin
from .models import *

# Register your models here.
class RegistroAdmin(admin.ModelAdmin):
    list_display = ['usuario', 'fecha_hora', 'entro', ]

    def save_model(self, request, obj, form, change):
        #aca guardarmos el usuario que creo la venta
        obj.usuario = request.user

        super().save_model(request, obj, form, change)

admin.site.register(Registro, RegistroAdmin)