from django.contrib import admin
from .models import *

class TermoAdmin(admin.ModelAdmin):
    list_display = ['nro', 'prestado', ]

admin.site.register(Termo, TermoAdmin)

class MateAdmin(admin.ModelAdmin):
    list_display = ['nro', 'prestado', ]

admin.site.register(Mate, MateAdmin)

def habilitar_cliente(modeladmin, request, queryset):
    for obj in queryset:
        obj.habilitado=True
        obj.save()

habilitar_cliente.short_description = 'Habilitar cliente'

class ClienteAdmin(admin.ModelAdmin):
    list_display = ['dni', 'nombre', 'email', 'telefono', 'habilitado', ]
    search_fields = ['dni', 'nombre', ]
    actions = [ habilitar_cliente, ]

admin.site.register(Cliente, ClienteAdmin)

def registrar_devolucion(modeladmin, request, queryset):
    for obj in queryset:
        obj.devuelto = True
        if obj.termo:
            termo = obj.termo
            termo.prestado = False
            termo.save()
        if obj.mate:
            mate = obj.mate
            mate.prestado = False
            mate.save()
        cliente = obj.cliente
        cliente.habilitado = True
        cliente.save()
        obj.save()

registrar_devolucion.short_description = 'Prestamo devuelto'

class PrestamoAdmin(admin.ModelAdmin):
    list_display = ['cliente', 'termo', 'mate', 'fecha_hora', 'aula', 'devuelto', ]
    raw_id_fields = ("cliente",)
    search_fields = ['cliente__nombre', 'termo__nro', 'mate__nro', ]
    actions = [ registrar_devolucion, ]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "termo":
            kwargs["queryset"] = Termo.objects.filter( prestado= False )
        if db_field.name == "mate":
            kwargs["queryset"] = Mate.objects.filter( prestado= False )
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        obj.cliente.habilitado = False
        obj.cliente.save()
        if form.cleaned_data['mate']==None:
            obj.termo.prestado = True
            obj.termo.save()
        elif form.cleaned_data['termo']==None:
            obj.mate.prestado = True
            obj.mate.save()
        else: 
            obj.mate.prestado = True
            obj.mate.save()
            obj.termo.prestado = True
            obj.termo.save()

        super(PrestamoAdmin, self).save_model(request, obj, form, change)


    def has_delete_permission(self, request, obj=None):
        pass

admin.site.disable_action('delete_selected')
admin.site.register(Prestamo, PrestamoAdmin)

