from django.db import models
from django.core.exceptions import ValidationError

class Termo(models.Model):
    nro = models.IntegerField( unique=True )
    prestado = models.NullBooleanField ( editable=False, default=False)


    def __str__(self):
        return str(self.nro)

    def delete(self, *args, **kwargs):
        prestamos = Prestamo.objects.filter(termo=self)
        for prestamo in prestamos:
            prestamo.cliente.habilitado=True
            prestamo.cliente.save()
            if (prestamo.mate!=None):
                prestamo.mate.prestado=False
                prestamo.mate.save()
        super(Termo, self).delete(*args, **kwargs)

class Mate(models.Model):
    nro = models.IntegerField( unique=True )
    prestado = models.NullBooleanField ( editable=False, default=False)


    def __str__(self):
        return str(self.nro)

    def delete(self, *args, **kwargs):
        prestamos = Prestamo.objects.filter(mate=self)
        for prestamo in prestamos:
            prestamo.cliente.habilitado=True
            prestamo.cliente.save()
            if (prestamo.termo!=None):
                prestamo.termo.prestado=False
                prestamo.termo.save()
        super(Mate, self).delete(*args, **kwargs)

class Cliente(models.Model):
    dni = models.BigIntegerField( primary_key = True )
    nombre = models.CharField ( max_length= 300 )
    email = models.EmailField( blank=True, null=True )
    telefono = models.BigIntegerField( blank=True, null=True )
    habilitado = models.BooleanField( default = True, editable=False, )

    def __str__(self):
        return str(self.dni) + ' ' + self.nombre
        
    def clean(self):
        if not self.email and not self.telefono:
            raise ValidationError("Tiene que tener por lo menos, algún dato de contacto.")

    def delete(self, *args, **kwargs):
        prestamos = Prestamo.objects.filter(cliente=self)
        for prestamo in prestamos:
            if (prestamo.termo!=None):
                prestamo.termo.prestado=False
                prestamo.termo.save()
            if (prestamo.mate!=None):
                prestamo.mate.prestado=False
                prestamo.mate.save()
        super(Cliente, self).delete(*args, **kwargs)

class Prestamo(models.Model):
    cliente = models.ForeignKey( Cliente, on_delete=models.CASCADE, )
    termo = models.ForeignKey( Termo, on_delete=models.CASCADE, blank=True, null=True, )
    mate = models.ForeignKey( Mate, on_delete=models.CASCADE, blank=True, null=True, )
    fecha_hora = models.DateField( auto_now_add=True )
    aula = models.CharField( max_length = 100, blank=True, null=True )
    devuelto = models.NullBooleanField ( editable=False, default=False)

    def clean(self):
        if not self.cliente.habilitado:
            raise ValidationError("El cliente no está habilitado para prestamos.")
        else:
            if not self.termo and not self.mate:
                raise ValidationError("Tiene que tener por lo menos, termo o mate.")

    def __str__(self):
        return self.cliente.nombre + '- Aula: ' + self.aula

    def delete(self, *args, **kwargs):
        if (self.termo != None):
            self.termo.prestado=False
            self.termo.save()
        if (self.mate!=None):
            self.mate.prestado=False
            self.mate.save()
        super(Prestamo, self).delete(*args, **kwargs)

