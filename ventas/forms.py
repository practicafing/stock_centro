from django import forms
from django.forms import ModelForm
from .models import *

class ProductoVentaForm(ModelForm):
    class Meta:
        model = ProductoVenta
        fields = '__all__'

    def clean_cantidad(self):
        cleaned_data = super().clean()
        cc_myself = cleaned_data.get("producto")
        assert False, cleaned_data.get("id")
        raise ValidationError('Too many characters ...' + str(cc_myself.id))

