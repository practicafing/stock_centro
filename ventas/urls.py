from django.urls import path
from . import views

urlpatterns = [
    path('', views.cierre_caja, name='cierre_caja'),
]
