from django.db import models
from producto.models import *
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from model_utils import FieldTracker
from django.core.validators import MaxValueValidator, MinValueValidator 


class Venta(models.Model):
	#caja = models.ForeignKey('Caja', on_delete=models.CASCADE)
	monto_total = models.FloatField(null=True, blank=True, default=0.0, editable=False)
	fecha = models.DateTimeField(auto_now_add=True)
	buffetero = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, editable=False)

	def __str__(self):
		return str(self.fecha)

	def delete(self, *args, **kwargs):
		prod_venta = ProductoVenta.objects.filter(venta=self)
		for pv in prod_venta:
			pv.producto.stock = pv.producto.stock + pv.cantidad
			pv.producto.save()	
		super(Venta, self).delete(*args, **kwargs)
#Producto + cantidad x venta
class ProductoVenta(models.Model):
	producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
	venta = models.ForeignKey(Venta, on_delete=models.CASCADE)
	cantidad = models.IntegerField()
    
	tracker = FieldTracker()

	def clean(self):
		if self.tracker.has_changed('cantidad'): #si cambio
			if self.tracker.previous('cantidad'): #si tenia previo valor
				if self.cantidad > self.producto.stock + self.tracker.previous('cantidad') or self.cantidad <0:
					raise ValidationError('Poné una cantidad válida')
				else:
					self.producto.stock = self.producto.stock + self.tracker.previous('cantidad') - self.cantidad
					self.venta.monto_total = self.venta.monto_total + self.producto.precio_venta*(self.cantidad - self.tracker.previous('cantidad'))
					self.producto.save()
					self.venta.save()
			else:
				if self.cantidad > self.producto.stock or self.cantidad<0:
					if self.venta.id != None:
						self.venta.delete()
					raise ValidationError('Poné una cantidad válida')
				else: 
					#self.producto.stock = self.producto.stock - self.cantidad
					self.venta.monto_total = self.venta.monto_total + self.producto.precio_venta*self.cantidad
					#self.producto.save()
					self.venta.save()
		else:
			pass

	def delete(self, *args, **kwargs):
		self.venta.monto_total = self.venta.monto_total - self.producto.precio_venta*self.cantidad
		self.venta.save()
		self.producto.stock = self.producto.stock + self.cantidad
		self.producto.save()
		super(ProductoVenta, self).delete(*args, **kwargs)

#todo en este modelo se calcula solo!
class Caja(models.Model):
	fecha = models.DateField( editable=False, )
	total = models.FloatField(null=True, blank=True, editable=False,  default=0.0,)
	ganancia = models.FloatField(null=True, blank=True, editable=False,  default=0.0,)
	costo = models.FloatField(null=True, blank=True, editable=False,  default=0.0,)
	descripcion = models.TextField(null=True, blank=True,)

	def __str__(self):
		return str(self.fecha)
