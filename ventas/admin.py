from django.contrib import admin, messages
# Register your models here.
from producto.models import *
from .models import *
from django.forms.models import BaseInlineFormSet
import datetime
from django import forms

class CajaAdmin(admin.ModelAdmin):
    list_display = ['fecha', 'total', 'ganancia', 'costo', 'descripcion']
    
    def has_add_permission(self, request):
        return False

admin.site.register(Caja, CajaAdmin)

class ProductoVentaInlineformset(forms.models.BaseInlineFormSet):

    def clean(self):
        lista = []
        cantidades = []
        productos = []
        for form in self.forms: #por cada producto venta
            #assert False, self.forms
            if ("producto" or "cantidad") in form.changed_data:
            #for field in form.changed_data: #obtengo todos los productoVenta de una venta y los asocio a un vector con su respectiva cantidad 
                #if field == "producto":
                productos.append(form.cleaned_data['producto'])
                cantidades.append(form.cleaned_data['cantidad'])

        #assert False, productos
        j = 0
        ban = 1
        #assert False, cantidades
        #activo una bandera que si es 1 significa que ninguna cantidad supero el stock de un producto venta.
        for p in productos:
            if p.stock < cantidades[j] or cantidades[j]<0:
                ban = 0
            j = j + 1

        j = 0
        if ban == 1: #si no hubo errores procedo a guardar la venta.

            prod_nueva = []
            cant_nueva = []
            #en este for se comprime el vector de productos y el de cantidades para que se guarde una sola vez y no haya problemas. 
            #ej. productos = [jugo, agua, turron, jugo], cantidades = [1, 1, 1, 1 ]
            #los vectores resultantes, serian: prod_nueva = [jugo, agua, turron], cant_nueva = [2, 1, 1]
            for i in productos: 
                if i not in prod_nueva:
                    prod_nueva.append(i)
                    cant_nueva.append(cantidades[j])
                    j = j + 1
                else:
                    cant_nueva[prod_nueva.index(i)] = cant_nueva[prod_nueva.index(i)] + cantidades[j]
                    j = j + 1
            j = 0

            #me fijo que las cantidades no superen al stock:
            for p in prod_nueva:
                if p.stock < cant_nueva[j]:
                    form.cleaned_data['venta'].delete()
                    raise ValidationError('Error - La cantidad supera al stock disponible')
                j = j + 1
            #actualizamos stock
            j=0
            for p in prod_nueva: 
                p.stock = p.stock -  cant_nueva[j]
                p.save()
                j = j+1
        else: 
            raise ValidationError('Hay errores en la venta')


#inline de los productos por venta
class ProductoVentaAdmin(admin.TabularInline):
    model = ProductoVenta
    #raw_id_fields = ("producto",)
    formset = ProductoVentaInlineformset

    extra=1

    def has_delete_permission(self, request, obj=None):
        pass

#admin de la venta!
class VentaAdmin(admin.ModelAdmin):
    inlines=[ProductoVentaAdmin,]
    list_display = ['fecha','monto_total', 'buffetero', ]

    def save_model(self, request, obj, form, change):
        #aca guardarmos el usuario que creo la venta
        obj.buffetero = request.user
        super().save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(VentaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(buffetero=request.user)

admin.site.register(Venta, VentaAdmin)
